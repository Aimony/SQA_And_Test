import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * @author Aimony
 * @date 2023/09/19 17:09
 * @describe 题目2：星期几问题
 * 已知公元1年1月1日是星期一，请编写一个程序，只要输入年月日，就能自动回答当天是星期几。
 */
public class WeekdayCalculator {
    public static void main(String[] args) {
        // 输入年、月、日
        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入年份：");
        int year = scanner.nextInt();

        System.out.print("请输入月份：");
        int month = scanner.nextInt();

        System.out.print("请输入日期：");
        int day = scanner.nextInt();

        // 构造日期对象
        LocalDate date = LocalDate.of(year, month, day);

        // 使用DateTimeFormatter将星期几转换为字符串
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE");
        String dayOfWeekString = date.format(formatter);

        System.out.println(year + "年" + month + "月" + day +" " + dayOfWeekString);
    }
}
