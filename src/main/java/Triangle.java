import java.util.Scanner;

/**
 * @author Aimony
 * @date 2023/09/19 16:56
 * @describe 题目1：三角形问题
 * 在三角形计算中，要求输入三角形的三个边长：A、B 和C。当三边不可能构成三角形时提示错误，可构成三角形时打印出信息，说明这个三角形是三边不等的、是等腰的、还是等边的。
 */
public class Triangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入第一条边长 (A): ");
        double a = scanner.nextDouble();

        System.out.print("请输入第二条边长 (B): ");
        double b = scanner.nextDouble();

        System.out.print("请输入第三条边长 (C): ");
        double c = scanner.nextDouble();

        scanner.close();

        // 检查是否可以构成三角形
        if (isTriangle(a, b, c)) {
            // 判断三角形类型
            String triangleType = classifyTriangle(a, b, c);
            System.out.println("这是一个 " + triangleType + " 三角形");
        } else {
            System.out.println("不是三角形");
        }
    }

    /**
     * 判断是否可以构成三角形
     *
     * @param a 边长
     * @param b 边长
     * @param c 边长
     * @return boolean
     */
    public static boolean isTriangle(double a, double b, double c) {
        return a + b > c && a + c > b && b + c > a;
    }

    /**
     * 判断三角形类型
     *
     * @param a 边长
     * @param b 边长
     * @param c 边长
     * @return {@link String}
     */
    public static String classifyTriangle(double a, double b, double c) {
        if (a == b && b == c) {
            return "等边";
        } else if (a == b || a == c || b == c) {
            return "等腰";
        } else {
            return "不等边";
        }
    }
}
